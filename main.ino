#include <SoftwareSerial.h>
#define RX 10
#define TX 11
String AP = "Alfred2";       // CHANGE ME
String PASS = "domo1234"; // CHANGE ME
String HOST = "10.0.0.1";
String PORT = "12999";
String BOARD_NAME = "led";
char command[] = "execute:ledon";
char command2[] = "execute:ledoff";
boolean initialized = false;

int ledPin = 13;

SoftwareSerial esp8266(RX,TX); 
int countTrueCommand;
int countTimeCommand; 
boolean found = false; 
void setup() {
  pinMode(ledPin, OUTPUT);
  // put your setup code here, to run once:
  Serial.begin(115200);
  esp8266.begin(9600);
  Serial.println("Test");
  sendCommand("AT",5,"OK");
  sendCommand("AT+CWMODE=1",5,"OK");
  sendCommand("AT+CWJAP=\""+ AP +"\",\""+ PASS +"\"",20,"OK");
  sendCommand("AT+CIPMUX=1",5,"OK");
  sendCommand("AT+CIPSTART=0,\"TCP\",\""+ HOST +"\","+ PORT,15,"OK");
}

void loop() {
  delay(1000);
  while (esp8266.available()) {
    if (!initialized) {
      initialize();
    }
    
    if (esp8266.find(command)) {
      Serial.println("Turning led 1 on");
      digitalWrite(ledPin, HIGH);
    }
    
    if (esp8266.find(command2)) {
      Serial.println("Turning led 1 off");
      digitalWrite(ledPin, LOW);
    }
  }
}

void initialize() {
  do {
    if (esp8266.find("getName")) {
      sendCommand("AT+CIPSEND=0," + String(BOARD_NAME.length()),4,">");
      esp8266.println(BOARD_NAME);
    }
    
    if (esp8266.find("initialize")) {
      Serial.println("Board initialized");
      initialized = true;
    }
  } while(!initialized);
}

void sendCommand(String command, int maxTime, char readReplay[]) {
  Serial.print(countTrueCommand);
  Serial.print(". at command => ");
  Serial.print(command);
  Serial.print(" ");
  while(countTimeCommand < (maxTime*1))
  {
    esp8266.println(command);//at+cipsend
    if(esp8266.find(readReplay))//ok
    {
      found = true;
      break;
    }
  
    countTimeCommand++;
  }
  
  if(found == true)
  {
    Serial.println("OYI");
    countTrueCommand++;
    countTimeCommand = 0;
  }
  
  if(found == false)
  {
    Serial.println("Fail");
    countTrueCommand = 0;
    countTimeCommand = 0;
  }
  
  found = false;
 }